<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function handlerRelation(){
        return $this->hasOne(Handlers::class, 'handler_id', 'handler_id');
    }

    public function packageRelation(){
        return $this->hasMany(Packages::class, 'service_id', 'service_id');
    }
}
