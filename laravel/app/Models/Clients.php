<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function barangayRelation(){
        return $this->hasOne(Barangays::class, 'barangay_id', 'barangay_id');
    }

    public function ordersRelation(){
        return $this->hasMany(Orders::class, 'clients_id', 'clients_id');
    }
}
