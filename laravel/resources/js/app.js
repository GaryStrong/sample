require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'

import router from './router/index'
Vue.component('home', require('./pages/LandingPage.vue'));

Vue.use(VueRouter)

const app = new Vue({
    el:'#app',
    router,
    components: { home }
})
