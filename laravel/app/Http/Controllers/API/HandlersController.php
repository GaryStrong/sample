<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\HandlersRequest;
use App\Http\Controllers\Controller;
use App\Models\Handlers;

class HandlersController extends Controller
{
    public function index()
    {
        $handlers = Handlers::all();

        return response()->json($handlers, 200);
    }
   
    public function store(HandlersRequest $request)
    {
        $handlers = Handlers::create($request->all());
        return response()->json($handlers, 201);
    }

    public function update(HandlersRequest $request, $id)
    {
        $handlers = Handlers::where('handler_id', $id)->update($request->all(), $id);
        return response()->json($handlers, 200);
    }
}
