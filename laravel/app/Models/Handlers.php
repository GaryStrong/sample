<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Handlers extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function serviceRelation(){
        return $this->hasMany(Services::class, 'handler_id', 'handler_id');
    }
}
