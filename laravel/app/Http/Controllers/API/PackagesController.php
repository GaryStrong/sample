<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PackagesRequest;
use App\Http\Controllers\Controller;
use App\Models\Packages;

class ServicesController extends Controller
{
    public function index()
    {
        $packages = DB::table('packages')
        ->join('services', 'services.service_id', '=', 'packages.service_id')
        ->select('packages.*', 'services.*')->get();

        return response()->json($packages, 200);
    }
   
    public function store(PackagesRequest $request)
    {
        $packages = Packages::create($request->all());
        return response()->json($packages, 201);
    }

    public function update(PackagesRequest $request, $id)
    {
        $packages = Packages::where('package_id', $id)->update($request->all(), $id);
        return response()->json($packages, 200);
    }
}