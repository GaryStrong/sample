<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrdersRequest;
use App\Http\Controllers\Controller;
use App\Models\Orders;

class ServicesController extends Controller
{
    public function index()
    {
        $orders = DB::table('orders')
        ->join('clients', 'clients.client_id', '=', 'orders.client_id')
        ->join('packages', 'packages.package_id', '=', 'orders.package_id')
        ->select('orders.*', 'clients.*', 'packages.*')->get();

        return response()->json($orders, 200);
    }
   
    public function store(OrdersRequest $request)
    {
        $orders = Orders::create($request->all());
        return response()->json($orders, 201);
    }

    public function update(OrdersRequest $request, $id)
    {
        $orders = Orders::where('order_id', $id)->update($request->all(), $id);
        return response()->json($orders, 200);
    }
}