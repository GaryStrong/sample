<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function serviceRelation(){
        return $this->hasOne(Services::class, 'service_id', 'service_id');
    }

    public function orderRelation(){
        return $this->hasMany(Orders::class, 'packages_id', 'packages_id');
    }
}
