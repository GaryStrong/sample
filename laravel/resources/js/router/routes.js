const routes = [
    {
        path: '/',
        component: () => import('../pages/LandingPage.vue'),
        name: 'home'
    }
]

export default routes;