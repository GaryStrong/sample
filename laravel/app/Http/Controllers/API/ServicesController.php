<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ServicesRequest;
use App\Http\Controllers\Controller;
use App\Models\Services;

class ServicesController extends Controller
{
    public function index()
    {
        $services = DB::table('services')
        ->join('handlers', 'handlers.handler_id', '=', 'services.handler_id')
        ->select('services.*', 'handlers.*')->get();

        return response()->json($services, 200);
    }
   
    public function store(ServicesRequest $request)
    {
        $services = Services::create($request->all());
        return response()->json($services, 201);
    }

    public function update(ServicesRequest $request, $id)
    {
        $services = Services::where('service_id', $id)->update($request->all(), $id);
        return response()->json($services, 200);
    }
}