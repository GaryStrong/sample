<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barangays extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function clientsRelation(){
        return $this->hasOne(Clients::class, 'barangay_id', 'barangay_id');
    }
}
