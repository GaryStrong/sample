<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\BarangaysController;
use App\Http\Controllers\API\ClientsController;
use App\Http\Controllers\API\HandlersController;
use App\Http\Controllers\API\OrdersController;
use App\Http\Controllers\API\PackagesController;
use App\Http\Controllers\API\ServicesController;




Route::apiResource('barangays', BarangaysController::class);
Route::apiResource('clients', ClientsController::class);
Route::apiResource('handlers', HandlersController::class);
Route::apiResource('services', ServicesController::class);
Route::apiResource('packages', PackagesController::class);
Route::apiResource('orders', OrdersController::class);
